﻿using System.Collections.Generic;
using Decoration;
using OtherModels;

namespace Models
{
    [GDPRTagging(GDPRTag.Z)]
    public class Models
    {     
        [GDPRTagging(GDPRTag.A)]
        public int IntProperty { get; set; }

        public int IntProperty2 { get; set; }

        [GDPRTagging(GDPRTag.B)]
        public string StringProperty { get; set; }
        
        [GDPRTagging(GDPRTag.A)]
        public InternalModel InternalModelPropertyProperty { get; set; } = new InternalModel();

        [GDPRTagging(GDPRTag.B)]
        public IList<InternalModel2> IListInternalModel2Property { get; set; } = new List<InternalModel2>();

        [GDPRTagging(GDPRTag.B)]
        public OtherModelA OtherModelAField { get; set; }

    }

    [GDPRTagging(GDPRTag.C)]
    public class InternalModel
    {
        public int IntProperty { get; set; }
        [GDPRTagging(GDPRTag.D)]
        public string StringProperty { get; set; }

        public IList<int> IntListProperty { get; set; } = new List<int>();
    }

    public class InternalModel2
    {
        public int IntProperty { get; set; }
        public string StringProperty { get; set; }

        //public Models Models { get; set; }

        [GDPRTagging(GDPRTag.F)]
        public IList<int> IntListProperty { get; set; } = new List<int>();
    }
}
