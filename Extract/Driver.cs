﻿using Decoration;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Newtonsoft.Json;
using System.IO;

namespace AssemblyExtraction
{
    class Driver
    {
        static void Main(string[] args)
        {
            var assembly = Assembly.LoadFrom(@"C:\Users\erikw\source\repos\test\ugh\Models\obj\Debug\netstandard2.0\Models.dll");
            var types = assembly.GetExportedTypes();
            
            Console.WriteLine("Available types: ");
            foreach (Type type in types)
            {
                Console.Write(type.Name + " ");
            }

            Console.WriteLine("Enter type name: ");
            var typeToCheck = Console.ReadLine();
            StreamWriter w = null;
            try
            {
                w = new StreamWriter("results.json");
                (new AttributeExtractor<GDPRTaggingAttribute>(assembly, typeToCheck)).Run(w);
            } 
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            } 
            finally
            {
                if (w != null) 
                    w.Dispose();
            }

        }    
    }
}
