﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssemblyExtraction
{
    public class AssemblyTypeInfo
    {
        public string AssemblyName { get; set; }
        //public string ParentName { get; set; }
        public string FieldName { get; set; }

        public string Type { get; set; }
        public List<string> Tags { get; private set; } = new List<string>();
    }
}
