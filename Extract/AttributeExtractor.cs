﻿using Decoration;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Newtonsoft.Json;
using System.IO;


namespace AssemblyExtraction
{
    public class AttributeExtractor<TAttribute> where TAttribute: GDPRTaggingAttribute
    {
        private readonly HashSet<Type> visitedTypes;
        private readonly Assembly baseAssembly;
        private readonly string baseTypeName;
        public AttributeExtractor(Assembly assembly, string typeName)
        {
            visitedTypes = new HashSet<Type>();
            baseAssembly = assembly;
            baseTypeName = typeName;
        }

        public void Run(StreamWriter w)
        {
            var typeToCheck = baseAssembly?.GetExportedTypes()?.FirstOrDefault(t => t.Name.Equals(baseTypeName));
            if (typeToCheck == null)
            {
                throw new Exception($"Did not find {typeToCheck} in assembly");
            }
            var assemblyInfos = GetTypeInfo(typeToCheck);
            var json = JsonConvert.SerializeObject(assemblyInfos, Formatting.Indented);
            w.Write(json);
        }

        private List<AssemblyTypeInfo> GetTypeInfo(Type type)
        {
            var assemblyInfos = new List<AssemblyTypeInfo>();
            if (type.IsGenericType)
            {
                throw new Exception($"Cannot process because {type.Name} is a generic type");
            }
            var appliedAttributeInstances = new List<TAttribute>();
            appliedAttributeInstances.AddRange(type.GetCustomAttributes<TAttribute>());
            foreach (var property in type.GetProperties())
            {
                TraversePropertyInfo(property, type.Name, appliedAttributeInstances, assemblyInfos);
            }
            AddAssemblyInfo(type, type.Name, appliedAttributeInstances, assemblyInfos);
            return assemblyInfos;
        }

        private bool TraversePropertyInfo(PropertyInfo propertyInfo, string fullParentName, List<TAttribute> appliedAttributes, List<AssemblyTypeInfo> assemblyInfos)
        {
            var type = propertyInfo.PropertyType;
            var fullFieldName = fullParentName + "." + propertyInfo.Name;
            var propertyTags = propertyInfo.GetCustomAttributes<TAttribute>();

            if (type.IsValueType || type == typeof(string))
            {
                return HandleValueType(type, fullFieldName, propertyTags, appliedAttributes, assemblyInfos);
            }

            if (type.IsGenericParameter || type.IsGenericTypeDefinition)
                return false;

            if (type.IsGenericType)
            {
                type = type.GetGenericArguments()[0];
                if (type.IsValueType || type == typeof(string))
                {
                    return HandleValueType(type, fullFieldName, propertyTags, appliedAttributes, assemblyInfos);
                }
            }

            if (visitedTypes.Contains(type))
            {
                throw new Exception($"Circular reference detected in {type.Name} - not supported");
            }

            visitedTypes.Add(type);

            var initialCount = appliedAttributes.Count;
            var typeDefinitionTags = type.GetCustomAttributes<TAttribute>();
            var newTagsToAdd = typeDefinitionTags.Union(propertyTags).Except(appliedAttributes).ToHashSet();
            appliedAttributes.AddRange(newTagsToAdd);

            bool allChildrenHaveNewTags = true;

            var childrenProperties = type.GetProperties();
            foreach (var childProperty in childrenProperties)
            {
                if (!TraversePropertyInfo(childProperty, fullFieldName, appliedAttributes, assemblyInfos))
                {
                    allChildrenHaveNewTags = false;
                }
            }

            if (newTagsToAdd.Any() || allChildrenHaveNewTags)
            {
                AddAssemblyInfo(type, fullFieldName, appliedAttributes, assemblyInfos);
            }
            appliedAttributes.RemoveRange(initialCount, newTagsToAdd.Count());
            return newTagsToAdd.Any() || allChildrenHaveNewTags;
        }

        private void AddAssemblyInfo(Type type, string fullName, List<TAttribute> appliedAttributes, List<AssemblyTypeInfo> assemblyInfos)
        {
            var assemblyInfo = new AssemblyTypeInfo() { AssemblyName = type.Assembly.FullName, FieldName = fullName, Type = type.Name };
            foreach (var tag in appliedAttributes)
            {
                assemblyInfo.Tags.Add(tag.ToString());
            }
            assemblyInfos.Add(assemblyInfo);
        }

        private bool HandleValueType(Type type, string fullFieldName, IEnumerable<TAttribute> tags, List<TAttribute> decorations, List<AssemblyTypeInfo> assemblyInfos)
        {
            var newTagsToAdd = tags.Except(decorations).ToHashSet();
            var initialCount = decorations.Count;
            if (newTagsToAdd.Any())
            {
                decorations.AddRange(newTagsToAdd);
                AddAssemblyInfo(type, fullFieldName, decorations, assemblyInfos);
                decorations.RemoveRange(initialCount, newTagsToAdd.Count());
                return true;
            }
            return false;
        }
    }
}
