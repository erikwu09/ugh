﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Decoration
{
    public enum GDPRTag
    {
        [Description("A")]
        A,
        [Description("B")]
        B,
        [Description("C")]
        C,
        [Description("D")]
        D,
        [Description("E")]
        E,
        [Description("F")]
        F,
        [Description("Z")]
        Z
    }

    public static class DecorationTypeExtension
    {
        public static string ToString(this GDPRTag decorationType)
        {
            return decorationType.GetDescription();
        }
    }
}
