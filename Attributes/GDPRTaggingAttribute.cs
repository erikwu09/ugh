﻿using System;

namespace Decoration
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = true)]
    public class GDPRTaggingAttribute: Attribute
    {
        public GDPRTag DecorationType { get; private set; }

        public GDPRTaggingAttribute(GDPRTag decoration)
        {
            DecorationType = decoration;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var decor = obj as GDPRTaggingAttribute;
            if (decor == null)
                return false;
            return decor.DecorationType == DecorationType;
        }

        public override int GetHashCode()
        {
            return DecorationType.GetHashCode();
        }

        public override string ToString()
        {
            return DecorationType.GetDescription();
        }
    }
}
